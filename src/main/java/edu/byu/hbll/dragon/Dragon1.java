/** */
package edu.byu.hbll.dragon;

import java.util.List;

/** */
public class Dragon1 {

  /** @param args */
  public static void main(String[] args) {

    System.out.println("start");

    ComplexClass complex = new ComplexClass();

    List<Object> data = complex.runSuperLongQueryOnDatabaseResidingInIndia();

    double result = complex.doUglyCalculusCalculationOnLongListOfObjects(data);

    complex.putResultInLocalDatabase("result", result);

    double average = complex.doSuperFastAverageOnListOfObjects(data);

    complex.putResultInLocalDatabase("average", average);

    System.out.println("finish");
    
  }
}
