/** */
package edu.byu.hbll.dragon;

import java.util.ArrayList;
import java.util.List;

/** @author Charles Draper */
public class ComplexClass {

  public List<Object> runSuperLongQueryOnDatabaseResidingInIndia() {
    try {
      Thread.sleep(400);
    } catch (InterruptedException e) {
    }

    return new ArrayList<>();
  }

  public double doUglyCalculusCalculationOnLongListOfObjects(List<Object> data) {
    try {
      Thread.sleep(50);
    } catch (InterruptedException e) {
    }

    return 3.215;
  }

  public void putResultInLocalDatabase(String name, double result) {
    try {
      Thread.sleep(5000);
    } catch (InterruptedException e) {
    }
  }

  public double doSuperFastAverageOnListOfObjects(List<Object> data) {
    try {
      Thread.sleep(1);
    } catch (InterruptedException e) {
    }

    return 2838.182;
  }
}
