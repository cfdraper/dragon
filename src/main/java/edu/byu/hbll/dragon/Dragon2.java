/** */
package edu.byu.hbll.dragon;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/** */
public class Dragon2 {

  /** @param args */
  public static void main(String[] args) {

    int[] list = new int[6];

    for (int i = 0; i < list.length; i++) {
      list[i] = i;
    }

    Collection<Long> results = new ArrayList<>();

    for (int i = 0; i < list.length; i++) {
      for (int j = 0; j < list.length; j++) {
        for (int k = 0; k < list.length; k++) {
          for (int l = 0; l < list.length; l++) {
            for (int m = 0; m < list.length; m++) {
              results.add((long) i * j * k * l * m);
            }
          }
        }
      }
    }

    List<Long> seeds = new ArrayList<>();

    Random r = new Random(1);

    for (int i = 0; i < 1000000; i++) {
      seeds.add((long) r.nextInt(seeds.size() + 1));
    }

    Collections.sort(seeds);

    int totalMatches = 0;

    for (Long seed : seeds) {
      if (results.contains(seed)) {
        totalMatches++;
      }
    }

    long maxSeed = Long.MIN_VALUE;

    for (Long seed : seeds) {
      maxSeed = Math.max(seed, maxSeed);
    }

    System.out.println("Total Matches: " + totalMatches + ", Largest Seed: " + maxSeed);
    
  }
}
